local component = require("component");
local sides = require("sides");
local rs = component.redstone;
local rate = 2.5;
local side = sides.left;
local powerOut = 10;
while (true) do
    os.sleep(rate);
    if (rs.getOutput(side) > 0) then
        rs.setOutput(side, 0);
    else
        rs.setOutput(side, powerOut);
    end
end